# Web Conversation Agent (Chatbot)
## 13 March 2022

### Prosa.ai Backend Engineer(NLP) Entrance Test

*P.T. Prosa Solusi Cerdas* <br />
*Jalan Dr. Otten No.10, Pasir Kaliki* <br />
*Kec. Cicendo Kota Bandung 40171, Indonesia* <br />

*2022*

## Description
Conversation Agent or Chatbot that returns an answer to a given question. The knowledge of the 
chatbot is based on a set of question and answer (QA) pairs. The general idea is that a given 
question will be answered using the answer of a similar question in QA pairs. <br>
To interact with the chatbot, users can submit their questions through a webpage. When users submit 
a question, it will be sent to the server. In the server, the question will be compared one by one 
with questions in QA pairs.If a similar question is found, the answer from its QA pair is returned 
to the user. If there is no similar question, the chatbot will return some message that tells 
the chatbot does not know the answer. When there is no response from users after a certain amount 
of time,the chatbot will send some greeting message to close the conversation. <br>

## Author
Reihan Andhika Putra
andhikareihan349@gmail.com
(+62)895-3839-95827 (Whatsapp)

## Requirements
- [Python 3](https://www.python.org/downloads/)

## Tech and Stack
1. Python
2. Flask
3. Sqlite
4. WebSocket

## Feature
1. Melakukan chatting dengan chatbot dengan memanfaatkan websocket
2. Penghitungan similaritas antara pertanyaan dengan menggunakan tf-idf dan cosine similarity (teks dilakukan preprocess terlebih dahulu)
3. API prediksi jawaban dengan proteksi menggunakan api key.
4. Chatbot dapat memberikan salam saat chat baru saja dimulai dan memberikan salam perpisahan apabila user sudah idle selama 30 detik atau konek websocket terputus
5. Referensi pertanyaan diambil dari faq prosa.ai 

## Installation And Run
Clone the repository
```bash
git clone https://gitlab.com/AndhikaRei/prosa.ai-backend-engineer-entrance-test.git
cd src
```
##### Make enviroment file by copying the environment example file.(IMPORTANT)
### Run With Python
#### First Time Setup
1. Open `setup.bat` or `setup.sh`, depend on your operating system.
2. Wait until the installation is finished
3. The setup will automatically open the web browser
4. If the page failed to load, wait a moment then refresh the page

#### Run
1. Open `run.bat`or `run.sh`, depend on your operating system.
2. It will automatically open the web browser
3. If the page failed to load, wait a moment then refresh the page

#### Manual Setup
After cloning the repository
```bash 
cd src
python -m venv virt
virt\Scripts\activate
pip install -r requirements.txt
flask db downgrade
flask db upgrade
flask seed run
flask run
```
Then open your web browser and go to [localhost:5000](http://localhost:5000)


### Run With Docker
Run these command
```
docker-compose -f ./deployments/compose/docker-compose.yml up --build
```
Then open your web browser and go to [localhost:5000](http://localhost:5000)

## Screen Capture 
### Greeting From Chatbot
![greeting](img/greeting.png)
### Valid Question
![valid](img/valid.png)
### Invalid Question
![invalid](img/invalid.png)
### Chatbot Close
![close](img/close.png)

## Reference
1. https://prosa.ai/faq : Sumber data
2. https://bbbootstrap.com/snippets/simple-chat-application-57631463 : Template
3. https://ksnugroho.medium.com/dasar-text-preprocessing-dengan-python-a4fa52608ffe : Materi belajar
4. http://www.tfidf.com : Materi belajar
5. https://medium.com/analytics-vidhya/best-nlp-algorithms-to-get-document-similarity-a5559244b23b : Materi belajar
6. https://flask-migrate.readthedocs.io/en/latest/#
7. https://flask-sqlalchemy.palletsprojects.com/en/2.x/#
8. https://flask-sock.readthedocs.io/en/latest/index.html
9. https://pypi.org/project/Flask-Seeder/
