import json

from matplotlib.font_manager import json_dump

class Message:
    def __init__(self, message, sender):
        self.sender = sender
        self.message = message