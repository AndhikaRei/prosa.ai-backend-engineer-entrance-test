from .. import db

class Question(db.Model):
    """
    Description:
    ------------
    Question is a class that contains the question and answer that stored in database.

    Attributes:
    -----------
    id: int
        The unique id of the question.
    question: str
        The question that the chatbot can answer. 
    answer: str
        The answer that the chatbot can reply.
    """
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(128), nullable=False)
    answer = db.Column(db.String(128), nullable=False)

    def __repr__(self) -> str:
        """
        Description:
        ------------
        Represent the question object in string.

        Returns:
        --------
        str
            The string representation of the question object.
        """
        return "Question=%s, Answer=%s" % (self.question, self.answer)
    