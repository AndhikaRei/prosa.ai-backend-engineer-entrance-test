# Import python modules
import math
from typing import List, Dict
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
from functools import reduce

# Import created modules.

# from src.models.question import Question
from src.models.question import Question

# Preprocessing variable initialization.
# Stemmer.
stemfactory = StemmerFactory()
stemmer = stemfactory.create_stemmer()
# Stopword Remover.
stopfactory = StopWordRemoverFactory()
stopword = stopfactory.create_stop_word_remover()

class Chatbot:
    """
    Description:
    ------------
    Chatbot is a class that contains the chatbot's algorithm.

    Attributes:
    -----------
    complete_documents: List[Question]
        The list of question that the chatbot can answer.
    documents: List[Question] 
        complete_documents that has been preprocessed.
    """

    def __init__(self, documents:List[Question] = []) -> None:
        """
        Description:
        ------------
        Initialize the chatbot.

        Parameters:
        -----------
        documents: List[Question]
            The list of question that the chatbot can answer.
        """
        
        if (not documents):
            # Query question object in database and fill the document with question.
            documents = Question.query.all()
        self.__complete_documents = documents
        self.__documents = [self.__preprocess(doc.question) for doc in documents]
        

    def predict(self, query:str) -> Question:
        """
        Description:
        ------------
        Predict the question that the user is asking.

        Parameters:
        -----------
        query: str
            The question that the user is asking.
        
        Returns:
        --------
        Question
            The question object that the user is asking, contain matched answer and question.
        """

        # Preprocessing query.
        query = self.__preprocess(query)

        # Find the most similar document.
        # Generate TF-IDF matix.
        all_document = self.__documents + [query]
        tfidf_matrix = TF_IDF(all_document).tf_idf
        
        # Calculate the similarity between query and documents.
        similarities:float = []
        for i in range(len(tfidf_matrix)-1):
            similarities.append(CosineSimilarity(tfidf_matrix[i], tfidf_matrix[-1]).similarity)   
        
        # Find the document with the highest similarity.
        max_sim = max(similarities)
        max_index = similarities.index(max_sim)

        # Return the answer based on accuracy.
        if (max_sim > 0.3):
            return self.__complete_documents[max_index]
        return None
            

    def __preprocess(self, sentence:str) -> str:
        """
        Description:
        ------------
        Preprocess sentence.

        Parameters:
        -----------
        sentence: str
            The sentence 
        """

        # Case folding.
        # Case folding is a process that convert all letters to lowercase, remove punctuation, etc.
        sentence = sentence.lower()
        sentence = sentence.split('.')[0]
        sentence = sentence.translate(str.maketrans('', '', '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'))

        # Remove stopword.
        # Stopword is a word that is not important for the question.
        sentence = stopword.remove(sentence)
        
        # Stemming.
        # Stemming is a process of reducing the word to its root.
        sentence = stemmer.stem(sentence)
        return sentence   

class TF_IDF:
    """
    Description:
    ------------
    TF_IDF is a class that compute TF-IDF matrix.

    Attributes:
    -----------
    documents: List[str]
        The list of documents.
    tf: List[Dict[str, float]]
        The list of TF matrix.
    idf: Dict[str, float]
        The IDF matrix.
    tf_idf: List[Dict[str, float]]
        The list of TF-IDF matrix.
    """

    def __init__(self, documents: List[str]) -> None:
        """
        Description:
        ------------
        Initialize TF_IDF.

        Parameters:
        -----------
        documents: List[str]
            The list of documents.
        """

        self.__documents:List[str] = documents
        self.__tf:List[Dict[str, float]] = []
        self.__idf:Dict[str, float] = []
        self.__tf_idf:List[Dict[str, float]] = []
    
    @property
    def tf_idf(self)->List[Dict[str, float]]:
        """
        Description:
        ------------
        Get TF-IDF matrix.

        Returns:
        --------
        List[Dict[str, float]]
            The list of TF-IDF matrix.
        """

        if not self.__tf_idf:
            self.__calculateTF()
            self.__calculateIDF()
            self.__calculateTF_IDF()
        return self.__tf_idf

    def __repr__(self) -> str:
        """
        Description:
        ------------
        Define string representation of TF_IDF.

        Returns:
        --------
        str
            The string representation of TF_IDF.
        """

        return str(self.tf_idf)

    def __calculateTF(self):
        """
        Description:
        ------------
        Calculate TF matrix.
        """

        # For each document, calculate the frequency of each word and divide by the total number 
        # of words in corresponding document.
        for document in self.__documents:
            num_words = len(document.split())
            tf_document = {}
            for word in document.split():
                tf_document[word] = tf_document.get(word, 0) + 1
            for word, count in tf_document.items():
                tf_document[word] = count / num_words
            self.__tf.append(tf_document)
    
    def __calculateIDF(self):
        """
        Description:
        ------------
        Calculate the idf matrix.
        """

        # For each word, calculate the number of documents that contain the word.
        # Get number of documents.
        n = len(self.__documents)
        idf = {}
        for document in self.__documents:
            for word in document.split():
                idf[word] = idf.get(word, 0) + 1
        # The idf value of a word is the logarithm of the ratio of the number of documents that 
        # contain the word to the total number of documents.
        for word, count in idf.items():
            idf[word] = math.log(n / count)
        self.__idf = idf
    
    def __calculateTF_IDF(self):
        """
        Description:
        ------------
        Calculate TF-IDF matrix.
        """

        # For each document, calculate the TF-IDF value of each word.
        # The TF-IDF value of a word in specific document is the multiplication of the TF value of 
        # the word in the corresponding document and the IDF value of the word.
        self.__tf_idf = []
        for tf in self.__tf:
            tf_idf_document = {}
            for word, value in tf.items():
                tf_idf_document[word] = value * self.__idf.get(word, 0)
            self.__tf_idf.append(tf_idf_document)

class CosineSimilarity():
    """
    Decription:
    -----------
    CosineSimilarity is a class that calculate the cosine similarity between two vectors.

    Attributes:
    -----------
    vector1: List[float]
        The first vector.
    vector2: List[float]
        The second vector.
    similarity: float
        The cosine similarity between two vectors.
    """

    def __init__(self, vec_1:Dict[str, float], vec_2:Dict[str, float]) -> None:
        """
        Description:
        ------------
        Initialize CosineSimilarity.

        Parameters:
        -----------
        vec_1: Dict[str, float]
            The first vector.
        vec_2: Dict[str, float]
            The second vector.
        """
        
        self.__vec_1:Dict[str, float] = vec_1
        self.__vec_2:Dict[str, float] = vec_2
        # Similarity initial value.
        self.__similarity:float = -1
    
    @property
    def similarity(self) -> float:
        """
        Description:
        ------------
        Get cosine similarity.

        Returns:
        --------
        float
            The cosine similarity between two vectors.
        """
        if self.__similarity == -1:
            self.__calculateSimilarity()
        return self.__similarity
    
    def __calculateSimilarity(self) -> None:
        """
        Description:
        ------------
        Calculate cosine similarity between two vectors.

        """
        
        # Variable initialization.
        dot_product:float = 0
        
        # Calculate length of vector 1, vector 2 and calculate dot product.
        vec_1_length = reduce(lambda x, y: x + y, [value**2 for value in self.__vec_1.values()])
        vec_2_length = reduce(lambda x, y: x + y, [value**2 for value in self.__vec_2.values()])
        for key in self.__vec_1.keys() & self.__vec_2.keys():
            dot_product += self.__vec_1.get(key, 0) * self.__vec_2.get(key, 0)
        
        # Calculate cosine similarity.
        # Default value.
        self.__similarity = 0 
        if vec_1_length != 0 and vec_2_length != 0:
            self.__similarity = dot_product / (math.sqrt(vec_1_length) * math.sqrt(vec_2_length))
