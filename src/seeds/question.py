from src.models.question import Question
from flask_seeder import Seeder

# Database Seeder.
class QuestionSeeder(Seeder):
    """
    Description:
    -----------
        Class for seeding the database.
    """
    
    def run(self):
        """
        Description:
        -----------
            Run the seeder.
        """
        # Create a list of questions.
        questions = [
            {
                'question': 'Apa yang membedakan Prosa.ai dengan perusahaan lain?',
                'answer': ( 'Prosa berasal dari kalimat "Pemrosesan Bahasa", kami adalah perusahaan ' 
                            'yang berdedikasi memberikan solusi berbasis teks dan suara khusus untuk ' 
                            'Bahasa Indonesia.' )   
            },
            {
                'question': 'Apakah layanan yang disediakan oleh Prosa.ai?',
                'answer': ( 'Layanan yang disediakan berupa API untuk pemrosesan teks dan pemrosesan ' 
                            'speech. Pemrosesan teks terdiri dari Syntactic Analyzer, ' 
                            'Word Normalizer, Named Entity Extractor, Sentiment Analyzer, ' 
                            'Quotation Extractor, Hate Speech Detector, dan Rude Words Classifier. ' 
                            'Pemrosesan speech terdiri dari Speech Recognizer, Speech Synthesizer, ' 
                            'Speaker Recognizer, dan Paralinguistic. Deskripsi lebih lengkap dapat ' 
                            'diakses di Prosa Text dan Prosa Speech. Penggunaan dari API ini bisa ' 
                            'menjadi produk akhir misalnya pembuatan chatbot, analisis sentimen, ' 
                            'ekstraksi informasi, deteksi identitas pembicara, pembacaan teks ' 
                            'otomatis, dll. Selain API, Prosa juga menyediakan solusi berdasarkan ' 
                            'kebutuhan konsumen. Untuk penjelasan lebih jelas, kamu bisa menghubungi ' 
                            'Prosa via email business@prosa.ai')
            },
            {
                'question': 'Apakah Prosa menyediakan layanan untuk pembelajaran terkait NLP atau AI? ',
                'answer': ( 'Untuk saat ini, Prosa belum menyediakan kursus dan semacamnya. Tetapi, ' 
                            'kamu bisa menggunakan layanan gratis API hingga 1000 API call.' )
            },
            {
                'question': 'Apakah saya bisa menghapus akun Prosa saya?',
                'answer': ( 'Untuk penghapusan akun, kamu bisa menghubungi kami via email ' 
                            'support@prosa.ai' )

            },
            {
                'question': 'Apa yang terjadi apabila saya tidak membayar tagihan Prosa? ',
                'answer': ( 'Kamu akan mendapatkan peringatan untuk membayar X hari sebelum jatuh ' 
                            'tempo tagihan. Apabila kamu tidak membayar, maka perangkat lunak yang ' 
                            'menggunakan API dari Prosa tidak akan dapat digunakan.' )
            },
            {
                'question': 'Bagaimana jika saya tidak menemukan produk yang saya cari? ',
                'answer': 'Kamu dapat mengontak kami di email support@prosa.ai '
            }, 
            {
                'question': ( 'Apabila saya memiliki proposal Tugas Akhir / Tesis / Penelitian terkait ' 
                              'dengan Natural Language Programming, bagaimana Prosa bisa membantu ' 
                              'saya?' ),
                'answer': ( 'Prosa menyediakan API pada Free Package yang bisa digunakan untuk ' 
                            'membandingkan hasil pekerjaan kamu dengan model yang digunakan Prosa. ' 
                            'Kamu bisa mendaftar sebagai member dan menggunakan fasilitas console ' 
                            'Prosa.' )
            }
        ]

        # Insert data to database.
        for question in questions:
            q = Question(**question)
            print("Adding question: %s" % q)
            self.db.session.add(q)
        self.db.session.commit()