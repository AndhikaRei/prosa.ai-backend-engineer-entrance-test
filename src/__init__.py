# Import python modules.
import imp
import os
import json
from typing import List
from flask import Config, Flask, session
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from flask_sock import Sock
from flask_migrate import Migrate
from flask_seeder import FlaskSeeder
from flask.json import jsonify
from dotenv import load_dotenv


# Import created modules.
from src.constants.http_status_codes import HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR
from src.models.message import Message

# Load Environment Variables.
load_dotenv()

# Global variables.
db = SQLAlchemy()
sock = Sock()
migrate = Migrate()
seeder = FlaskSeeder()
# sess = Session()
messages_temp = []
SESSION_TYPE = os.environ.get('SESSION_TYPE', "filesystem")

def create_app(config: Config = None) -> Flask:
    """
    Description:
    ------------
    Create the flask app.

    Parameters:
    -----------
    config: Config
        The configuration of the flask app.

    Returns:
    --------
    Flask
        The flask app.
    """

    # Initialize the flask app with factory patterns.
    app = Flask(__name__)
    
    # Add configuration to the flask app depending on input and environment. 
    if config is None:
        app.config.from_mapping(
            # Flask Configuration.
            DEBUG = os.environ.get('DEBUG', True),
            FLASK_ENV = os.environ.get('FLASK_ENV', 'development'),
            FLASK_APP = os.environ.get('FLASK_APP', 'app.py'),
            SECRET_KEY = os.environ.get('SECRET_KEY', "mysecret"),
            UPLOAD_FOLDER = os.environ.get('UPLOAD_FOLDER', "./static/uploads"),
            THREADED = os.environ.get('THREADED', False),

            # Session Configuration.
            # SESSION_TYPE = os.environ.get('SESSION_TYPE', "filesystem"),
            # SESSION_COOKIE_SECURE = os.environ.get('SESSION_COOKIE_SECURE', True),
            # SESSION_COOKIE_SAMESITE = os.environ.get('SESSION_COOKIE_SAMESITE', "None"),

            # SQLAlchemy Configuration.
            SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI', "sqlite:///app.db"),
            SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS', False),

            # Other Configuration.
            PROSA_API_KEY= os.environ.get('PROSA_API_KEY', ""),
        )
    SESSION_TYPE = os.environ.get('SESSION_TYPE', "filesystem")

    # Inject the global variable with flaks app.
    db.init_app(app)
    sock.init_app(app)
    migrate.init_app(app, db)
    seeder.init_app(app, db)
    # sess.init_app(app)
    
    return app