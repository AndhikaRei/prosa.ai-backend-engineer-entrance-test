# Import python modules.
import json
import os
import requests
from flask import Blueprint, jsonify, request, session

# Import created modules.
from .. import sock, messages_temp
from src.constants.url import URL_BASE
from src.models.message import Message
from src.constants.http_status_codes import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED
from src.services.chatbot import Chatbot

# Make the blueprint.
api_route = Blueprint('api_route', __name__, url_prefix='/api/v1')

"""
--------------------------------------------------------------
# Message Api Routes.
--------------------------------------------------------------
"""

@sock.route('/api/v1/messages')
def message(ws):
    """
    Description:
    ------------
    Send a message to the server with websocket.

    Parameters:
    -----------
    ws: websocket
        The websocket that is connected to the server.
    
    Path:
    -----
    ws /api/v1/messages
    """
    while True:
        # Receive the message from the client.
        jsonResponse = json.loads(ws.receive())
        message:str = jsonResponse['message']

        # Get bot response from prosa external services.
        # Construct request header.
        headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'ProsaApiKey': os.environ.get('PROSA_API_KEY', "")
        }
        # Construct request body.
        body = {
            'message': message
        }
        # Send request to prosa external services and parse the response.
        response = requests.post(url=URL_BASE + "/api/v1/prosa.ai/predict", 
            headers=headers, json=body)
        response = response.json()

        # Construct the message object from bot and user chat.
        botResponse = response['data']['answer']
        userChat = Message(message, 'user')
        botChat = Message(botResponse, 'bot')

        # Send bot message to client.
        ws.send(json.dumps({
            'apiVersion': '1.0',
            'data': {
                'code': HTTP_200_OK,
                'message': botChat.message,
                'sender': botChat.sender
            }
        }))

"""
--------------------------------------------------------------
# Message Prediction Routes.
# Imagine that this is a real prosa services API.
--------------------------------------------------------------
"""
@api_route.route('/prosa.ai/predict', methods=['POST'])
def predict():
    """
    Description:
    ------------
    Predict the message from the body. Return the predicted message.

    Path:
    -----
    POST /api/v1/prosa.ai/predict

    Request Header:
    ---------------
    Content-Type: application/json
    Accept: application/json
    ProsaApiKey: <your prosa api key>

    
    Request Body:
    -------------
    message: str (required)
    """

    # Validate request api key in header.
    # Imagine this is real prosa services API validation.
    if request.headers.get('ProsaApiKey') != "aaf3f1d2-1ca8-4944-82e1-a2b7f9520f82":
        return jsonify({
        'apiVersion': '1.0',
        'error': {
            'code': HTTP_401_UNAUTHORIZED,
            'message': 'Invalid api key'
        }
    }), HTTP_401_UNAUTHORIZED

    # Validate request body.
    requestBody = request.get_json()
    if 'message' not in requestBody:
        return jsonify({
        'apiVersion': '1.0',
        'error': {
            'code': HTTP_400_BAD_REQUEST,
            'message': 'Invalid request body'
        }
    }), HTTP_400_BAD_REQUEST

    # Process request message.
    message = requestBody['message']
    chatbot = Chatbot()
    botResponse = chatbot.predict(message)

    # TODO: fix session.
    # sess = session['messages']
    # sess.append([message, 'user'])
    messages_temp.append([message, 'user'])
    
    # Return message based on response.
    if (not botResponse):
        # TODO: fix session
        # sess.append(["Mohon maaf, saya tidak mengerti pertanyaan anda", "bot"])
        # session['messages'] = sess
        # session.modified = True
        messages_temp.append(["Mohon maaf, saya tidak mengerti pertanyaan anda", "bot"])
        return jsonify({
            'apiVersion': '1.0',
            'data': {
                'answer': "Mohon maaf, saya tidak mengerti pertanyaan anda.",
                'question': ""
            }
        }), HTTP_200_OK
    
    # TODO: fix session
    # sess.append([botResponse.answer, "bot"])
    # session['messages'] = sess
    # session.modified = True
    messages_temp.append([botResponse.answer, "bot"])
    return jsonify({
        'apiVersion': '1.0',
        'data': {
            'answer': botResponse.answer,
            'question': botResponse.question
        }
    }), HTTP_200_OK