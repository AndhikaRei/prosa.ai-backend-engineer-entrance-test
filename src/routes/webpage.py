# Import python modules.
from flask import Blueprint, render_template, session

# Import created modules.
from .. import messages_temp

# Make the blueprint.
webpage_route = Blueprint('webpage_route', __name__, url_prefix='/')


"""
--------------------------------------------------------------
# Render Chatbot Webpages Routes (Frontend)
--------------------------------------------------------------
"""
@webpage_route.route("/chatbot")
def chatbot():
    """
    Description:
    ------------
    Render the chatbot webpage.

    Path:
    -----
    GET /chatbot

    """

    # TODO: fix session
    # Get message data from session.
    # messages = session.get('messages', [])
    # if not session.get('messages'):
    #     session['messages'] = []
    #     messages = messages_temp
    # session.modified = True  

    messages = messages_temp
    # Render the webpage.
    return render_template('pages/chatbot.html', messages=messages)